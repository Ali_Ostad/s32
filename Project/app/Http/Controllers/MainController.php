<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view("home");
    }

    public function Session(Request $request)
    {
//        $request->session()->put('users' , ['Ali' , 'hasan' , 'jalil']);
//        $request->session()->push('users' , 'hosein');
//        $request->session()->flash('users' , 'hosein');
//        return $request->session()->flash('users' , 'ali');
//        $request->session()->reflash();

    }


    public function session2(Request $request)
    {
//        dd($request->session()->all('users'));
    }

    public function setflash(Request $request)
    {
//        $request->session()->flash('name' , 'ali');
//
//        return $request->session()->all();
    }

    public function allSession(Request $request)
    {
//        return $request->session()->all();
    }


    public function UploadFile(Request $request)
    {
        // validation
        $path = $request->file('file')->store('files');
        $name = $request->file('file')->getClientOriginalName();
        $request->session()->flash('upload', $name);

        return redirect()->route('home');
    }
}
