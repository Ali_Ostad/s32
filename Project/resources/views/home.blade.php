@extends('layouts.index')

@section('content')
    @if(session()->exists('upload'))

        <div class="flash-Message">
            {{trans("user.file" , ['name' => session()->get('upload')])}}
        </div>

    @endif

    <form  action="{{route('upload')}}" enctype="multipart/form-data" method="post">
        @csrf
        <h2>Form Get Data</h2>


        <div class="form-group">
            <lable for="da">file :</lable>
            <input class="form-control" type="file" name="file">
        </div>
        <input class="btn btn-info" type="submit">
    </form>

@endsection
