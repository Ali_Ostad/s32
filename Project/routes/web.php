<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/' , 'MainController@index')->name('home');

//Route::get('/session' , 'MainController@session');

//Route::get('/session2' , 'mainController@session2');


//Route::get('/setFlash' , 'MainController@setflash');
//Route::get('/showAllSession' , 'MainController@allSession');

Route::get('/', function () {
    return view('home');
})->name('home');

Route::post('/upload-file', 'MainController@UploadFile')->name('upload');
